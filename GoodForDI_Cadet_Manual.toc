\select@language {english}
\contentsline {section}{\numberline {1}\relax \fontsize {17.28}{22}\selectfont Specifications}{4}
\contentsline {section}{\numberline {2}\relax \fontsize {17.28}{22}\selectfont Server Setup}{5}
\contentsline {subsection}{\numberline {2.1}LAMP Setup}{5}
\contentsline {subsection}{\numberline {2.2}Installing Packages}{5}
\contentsline {subsection}{\numberline {2.3}Overdue Passes}{6}
\contentsline {section}{\numberline {3}\relax \fontsize {17.28}{22}\selectfont Cadet Roster}{7}
\contentsline {subsection}{\numberline {3.1}Roster Page}{7}
\contentsline {section}{\numberline {4}\relax \fontsize {17.28}{22}\selectfont User Settings}{8}
\contentsline {subsection}{\numberline {4.1}View Previous Passes}{8}
\contentsline {subsection}{\numberline {4.2}Update Contact Info}{8}
\contentsline {subsection}{\numberline {4.3}Change Password}{8}
\contentsline {section}{\numberline {5}\relax \fontsize {17.28}{22}\selectfont Admin Settings}{9}
\contentsline {subsection}{\numberline {5.1}Modify Unit Assignments}{9}
\contentsline {subsubsection}{\numberline {5.1.1}Unit Admin}{9}
\contentsline {paragraph}{\numberline {5.1.1.1}Adding users}{9}
\contentsline {paragraph}{\numberline {5.1.1.2}Removing users}{10}
\contentsline {paragraph}{\numberline {5.1.1.3}Elevate Privileges/Remove Privileges}{10}
\contentsline {subsubsection}{\numberline {5.1.2}Super User}{10}
\contentsline {paragraph}{\numberline {5.1.2.1}Adding users}{10}
\contentsline {subsection}{\numberline {5.2}Modify Announcements}{11}
\contentsline {subsection}{\numberline {5.3}Reset a Password}{11}
\contentsline {subsection}{\numberline {5.4}View Previous Passes}{11}
\contentsline {subsection}{\numberline {5.5}Open and Close Passes}{11}
\contentsline {subsubsection}{\numberline {5.5.1}Pass Change}{11}
\contentsline {subsection}{\numberline {5.6}Recalls}{11}
\contentsfinish 
